﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using MySql.Data.MySqlClient;
using salesassistant.Model;

namespace salesassistant.Database
{
	public class SalesAssistantContext
	{
		public String ConnectionString { get; set; }
		public String ImagesConnectionString { get; set; }
		static CloudBlobClient blobClient;
		const string blobContainerName = "salesassistantimages";
        static CloudBlobContainer blobContainer;
		static string bloburl = "https://salesassistantstorage.blob.core.windows.net/salesassistantimages/";

		public SalesAssistantContext(string connectionString, string imagesConnectionString)
		{
			this.ConnectionString = connectionString;
			this.ImagesConnectionString = imagesConnectionString;
		}

		private MySqlConnection GetConnection()
		{
			return new MySqlConnection(ConnectionString);
		}

		public User GetUser(string email)
        {
			User user = null;
            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
				MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.users WHERE salesassistantdb.users.email = \"" + email + "\"", connection);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
						user = new User()
                        {
                            Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
                            Email = reader["email"] is DBNull ? null : reader.GetString("email"),
							Access = reader["access"] is DBNull ? false : reader.GetBoolean("access")
                        };
                    }
                }

            }
			return user;
        }

		public User GetUserPassword(string email, string password)
        {
            User user = null;
            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
				MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.users WHERE salesassistantdb.users.email = \"" + email 
				                                    + "\" AND salesassistantdb.users.password = \"" + password + "\"", connection);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        user = new User()
                        {
                            Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
                            Email = reader["email"] is DBNull ? null : reader.GetString("email"),
							Password = reader["password"] is DBNull ? null : reader.GetString("password"),
                            Access = reader["access"] is DBNull ? false : reader.GetBoolean("access")
                        };
                    }
                }

            }
            return user;
        }

		public void SaveUser(User user)
		{
			using (MySqlConnection connection = GetConnection())
			{
				connection.Open();
				MySqlCommand cmd = new MySqlCommand("INSERT INTO salesassistantdb.users " +
													"(email, password, access) VALUES (@email, @password, @access)", connection);
				cmd.Parameters.Add(new MySqlParameter
				{
					ParameterName = "@email",
					DbType = DbType.String,
					Value = user.Email
				});
				cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@password",
                    DbType = DbType.String,
                    Value = user.Password
                });
				cmd.Parameters.Add(new MySqlParameter
				{
					ParameterName = "@access",
					DbType = DbType.Boolean,
					Value = user.Access
				});
				cmd.ExecuteNonQuery();
			}
		}

		public List<User> GetUsers()
        {
			List<User> users = new List<User>();
            User user = null;
            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
				MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.users", connection);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        user = new User()
                        {
                            Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
                            Email = reader["email"] is DBNull ? null : reader.GetString("email"),
                            Access = reader["access"] is DBNull ? false : reader.GetBoolean("access")
                        };
						users.Add(user);
                    }
                }

            }
            return users;
        }

		public void DeleteUser(User user)
        {
            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("DELETE FROM salesassistantdb.users " +
                                                    "WHERE email=\"" + user.Email + "\"", connection);
                cmd.ExecuteNonQuery();
            }
        }

		public void UpdateUsers(List<User> users)
		{
			using (MySqlConnection connection = GetConnection())
			{
				connection.Open();
				users.ForEach(usr =>
				{
					int boolResult = usr.Access ? 1 : 0;
					MySqlCommand cmd = new MySqlCommand("UPDATE salesassistantdb.users " +
														"SET access=" + boolResult + " " +
														"WHERE email=\"" + usr.Email + "\"", connection);
					cmd.ExecuteNonQuery();
				});
			}
		}
        
		public void UpdateUserPassword(User user)
        {
            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE salesassistantdb.users " +
				                                    "SET password=\"" + user.Password + "\" " +
				                                    "WHERE email=\"" + user.Email + "\"", connection);
                cmd.ExecuteNonQuery();
            }
        }

		public ExCo GetExCo(string key)
		{
			ExCo exCo = null;
			using (MySqlConnection connection = GetConnection())
			{
				connection.Open();
				MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.exco WHERE salesassistantdb.exco.name = \"" + key + "\"", connection);
				using (MySqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						exCo = new ExCo()
						{
							Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
							Name = reader["name"] is DBNull ? null : reader.GetString("name"),
							Description = reader["description"] is DBNull ? null : reader.GetString("description"),
							ImageUri = reader["image"] is DBNull ? null : new Uri(reader.GetString("image"))
						};
						exCo.NameParts = exCo.Name.Split(' ').OfType<string>().ToList();
					}
				}
			}
			return exCo;
		}

		public List<ExCo> GetExCos()
		{
			List<ExCo> excos = new List<ExCo>();
			using (MySqlConnection connection = GetConnection())
			{
				connection.Open();
				ExCo exCo;
				MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.exco order by name asc", connection);
				using (MySqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						exCo = new ExCo()
						{
							Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
							Name = reader["name"] is DBNull ? null : reader.GetString("name"),
							Description = reader["description"] is DBNull ? null : reader.GetString("description"),
							ImageUri = reader["image"] is DBNull ? null : new Uri(reader.GetString("image")),
							coeList = new List<CoE>()
						};
						exCo.NameParts = exCo.Name.Split(' ').OfType<string>().ToList();
						excos.Add(exCo);
					}
				}

			}
			return excos;
		}

		public CoE GetCoE(string CoEName)
		{
			CoE coe = null;
			using (MySqlConnection connection = GetConnection())
			{
				connection.Open();
				MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.coe WHERE salesassistantdb.coe.name = \"" + CoEName + "\"", connection);
				using (MySqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						coe = new CoE()
						{
							Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
							Name = reader["name"] is DBNull ? null : reader.GetString("name"),
							Description = reader["description"] is DBNull ? null : reader.GetString("description"),
							ExcoName = reader["exco_name"] is DBNull ? null : reader.GetString("exco_name"),
							ImageUri = reader["image"] is DBNull ? null : new Uri(reader.GetString("image")),
							productList = new List<Product>()
						};
					}
				}

			}
			return coe;
		}

		public List<CoE> GetCoEs(string ExcoName)
		{
			List<CoE> coes = new List<CoE>();
			using (MySqlConnection connection = GetConnection())
			{
				connection.Open();
				CoE coe;
				MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.coe WHERE salesassistantdb.coe.exco_name = \"" + ExcoName + "\"", connection);
				using (MySqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						coe = new CoE()
						{
							Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
							Name = reader["name"] is DBNull ? null : reader.GetString("name"),
							Description = reader["description"] is DBNull ? null : reader.GetString("description"),
							ExcoName = reader["exco_name"] is DBNull ? null : reader.GetString("exco_name"),
							ImageUri = reader["image"] is DBNull ? null : new Uri(reader.GetString("image")),
							productList = new List<Product>()
						};
						coes.Add(coe);
					}
				}

			}
			return coes;
		}

		public Product GetProduct(string ProductName)
		{
			Product product = null;
			using (MySqlConnection connection = GetConnection())
			{
				connection.Open();
				MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.product WHERE salesassistantdb.product.service_name = \"" + ProductName + "\"", connection);
				using (MySqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						product = new Product()
						{
							Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
							COEName = reader["coe_name"] is DBNull ? null : reader.GetString("coe_name"),
							ExcoName = reader["exco_name"] is DBNull ? null : reader.GetString("exco_name"),
							ServiceName = reader["service_name"] is DBNull ? null : reader.GetString("service_name"),
							OwnerOneName = reader["owner_one_name"] is DBNull ? null : reader.GetString("owner_one_name"),
							OwnerOneEmail = reader["owner_one_email"] is DBNull ? null : reader.GetString("owner_one_email"),
							OwnerOneNumber = reader["owner_one_number"] is DBNull ? null : reader.GetString("owner_one_number"),
							OwnerTwoName = reader["owner_two_name"] is DBNull ? null : reader.GetString("owner_two_name"),
							OwnerTwoEmail = reader["owner_two_email"] is DBNull ? null : reader.GetString("owner_two_email"),
							OwnerTwoNumber = reader["owner_two_email"] is DBNull ? null : reader.GetString("owner_two_number"),
							ElevatorPitch = reader["elevator_pitch"] is DBNull ? null : reader.GetString("elevator_pitch"),
							ServiceOfferingDescription = reader["service_offering_description"] is DBNull ? null : reader.GetString("service_offering_description"),
							ProblemSolved = reader["problem_solved"] is DBNull ? null : reader.GetString("problem_solved"),
							ValueProposition = reader["value_proposition"] is DBNull ? null : reader.GetString("value_proposition"),
							CustomersAndProjects = reader["customers_and_projects"] is DBNull ? null : reader.GetString("customers_and_projects"),
							Services = reader["services"] is DBNull ? null : reader.GetString("services"),
							Submitter = reader["submitter"] is DBNull ? "" : reader.GetString("submitter"),
							ImageUri = reader["image"] is DBNull ? null : new Uri(reader.GetString("image"))
						};
					}
				}
			}
			return product;
		}

		public List<Product> GetProductsInExco(string excoName)
        {
            List<Product> products = new List<Product>();
            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
                Product product;
				MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.product WHERE salesassistantdb.product.exco_name = \"" + excoName + "\"", connection);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        product = new Product()
                        {
                            Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
                            COEName = reader["coe_name"] is DBNull ? "" : reader.GetString("coe_name"),
                            ExcoName = reader["exco_name"] is DBNull ? "" : reader.GetString("exco_name"),
                            ServiceName = reader["service_name"] is DBNull ? "" : reader.GetString("service_name"),
                            OwnerOneName = reader["owner_one_name"] is DBNull ? "" : reader.GetString("owner_one_name"),
                            OwnerOneEmail = reader["owner_one_email"] is DBNull ? "" : reader.GetString("owner_one_email"),
                            OwnerOneNumber = reader["owner_one_number"] is DBNull ? "" : reader.GetString("owner_one_number"),
                            OwnerTwoName = reader["owner_two_name"] is DBNull ? "" : reader.GetString("owner_two_name"),
                            OwnerTwoEmail = reader["owner_two_email"] is DBNull ? "" : reader.GetString("owner_two_email"),
                            OwnerTwoNumber = reader["owner_two_email"] is DBNull ? "" : reader.GetString("owner_two_number"),
                            ElevatorPitch = reader["elevator_pitch"] is DBNull ? "" : reader.GetString("elevator_pitch"),
                            ServiceOfferingDescription = reader["service_offering_description"] is DBNull ? "" : reader.GetString("service_offering_description"),
                            ProblemSolved = reader["problem_solved"] is DBNull ? "" : reader.GetString("problem_solved"),
                            ValueProposition = reader["value_proposition"] is DBNull ? "" : reader.GetString("value_proposition"),
                            CustomersAndProjects = reader["customers_and_projects"] is DBNull ? "" : reader.GetString("customers_and_projects"),
                            Services = reader["services"] is DBNull ? "" : reader.GetString("services"),
                            Version = reader["version"] is DBNull ? 0 : reader.GetInt32("version"),
                            Submitter = reader["submitter"] is DBNull ? "" : reader.GetString("submitter"),
                            ImageUri = reader["image"] is DBNull ? null : new Uri(reader.GetString("image"))
                        };
                        products.Add(product);
                    }
                }
            }
            return products;
        }

		public List<Product> GetProducts(string COEName)
		{
			List<Product> products = new List<Product>();
			using (MySqlConnection connection = GetConnection())
			{
				connection.Open();
				Product product;
				MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.product WHERE salesassistantdb.product.coe_name = \"" + COEName + "\"", connection);
				using (MySqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						product = new Product()
						{
							Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
							COEName = reader["coe_name"] is DBNull ? "" : reader.GetString("coe_name"),
							ExcoName = reader["exco_name"] is DBNull ? "" : reader.GetString("exco_name"),
							ServiceName = reader["service_name"] is DBNull ? "" : reader.GetString("service_name"),
							OwnerOneName = reader["owner_one_name"] is DBNull ? "" : reader.GetString("owner_one_name"),
							OwnerOneEmail = reader["owner_one_email"] is DBNull ? "" : reader.GetString("owner_one_email"),
							OwnerOneNumber = reader["owner_one_number"] is DBNull ? "" : reader.GetString("owner_one_number"),
							OwnerTwoName = reader["owner_two_name"] is DBNull ? "" : reader.GetString("owner_two_name"),
							OwnerTwoEmail = reader["owner_two_email"] is DBNull ? "" : reader.GetString("owner_two_email"),
							OwnerTwoNumber = reader["owner_two_email"] is DBNull ? "" : reader.GetString("owner_two_number"),
							ElevatorPitch = reader["elevator_pitch"] is DBNull ? "" : reader.GetString("elevator_pitch"),
							ServiceOfferingDescription = reader["service_offering_description"] is DBNull ? "" : reader.GetString("service_offering_description"),
							ProblemSolved = reader["problem_solved"] is DBNull ? "" : reader.GetString("problem_solved"),
							ValueProposition = reader["value_proposition"] is DBNull ? "" : reader.GetString("value_proposition"),
							CustomersAndProjects = reader["customers_and_projects"] is DBNull ? "" : reader.GetString("customers_and_projects"),
							Services = reader["services"] is DBNull ? "" : reader.GetString("services"),
							Version = reader["version"] is DBNull ? 0 : reader.GetInt32("version"),
							Submitter = reader["submitter"] is DBNull ? "" : reader.GetString("submitter"),
							ImageUri = reader["image"] is DBNull ? null : new Uri(reader.GetString("image"))
						};
						products.Add(product);
					}
				}
			}
			return products;
		}

		public void AddProduct(Product product)
		{
			using (MySqlConnection connection = GetConnection())
			{
				connection.Open();
				MySqlCommand cmd = new MySqlCommand("INSERT INTO salesassistantdb.product " +
													"(coe_name, exco_name, service_name, owner_one_name, owner_one_email, owner_one_number, " +
													"owner_two_name, owner_two_email, owner_two_number, elevator_pitch, service_offering_description, " +
													"problem_solved, value_proposition, customers_and_projects, services, version, submitter) " +
													"values" +
													"(@coe_name, @exco_name, @service_name, @owner_one_name, @owner_one_email, @owner_one_number, " +
													"@owner_two_name, @owner_two_email, @owner_two_number, @elevator_pitch, @service_offering_description, " +
													"@problem_solved, @value_proposition, @customers_and_projects, @services, @version, @submitter)"
													, connection);
				cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@coe_name",
                    DbType = DbType.String,
					Value = product.COEName
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@exco_name",
                    DbType = DbType.String,
					Value = product.ExcoName
                });
				cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@service_name",
                    DbType = DbType.String,
					Value = product.ServiceName
                });
				cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@owner_one_name",
                    DbType = DbType.String,
					Value = product.OwnerOneName
                });
				cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@owner_one_email",
                    DbType = DbType.String,
					Value = product.OwnerOneEmail
                });
				cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@owner_one_number",
                    DbType = DbType.String,
					Value = product.OwnerOneNumber
                });
				cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@owner_two_name",
                    DbType = DbType.String,
					Value = product.OwnerTwoName
                });
				cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@owner_two_email",
                    DbType = DbType.String,
					Value = product.OwnerTwoEmail
                });
				cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@owner_two_number",
                    DbType = DbType.String,
					Value = product.OwnerTwoNumber
                });
				cmd.Parameters.Add(new MySqlParameter
                {
					ParameterName = "@elevator_pitch",
                    DbType = DbType.String,
					Value = product.ElevatorPitch
                });
				cmd.Parameters.Add(new MySqlParameter
                {
					ParameterName = "@service_offering_description",
                    DbType = DbType.String,
					Value = product.ServiceOfferingDescription
                });
				cmd.Parameters.Add(new MySqlParameter
                {
					ParameterName = "@problem_solved",
                    DbType = DbType.String,
					Value = product.ProblemSolved
                });
				cmd.Parameters.Add(new MySqlParameter
                {
					ParameterName = "@value_proposition",
                    DbType = DbType.String,
					Value = product.ValueProposition
                });
				cmd.Parameters.Add(new MySqlParameter
                {
					ParameterName = "@customers_and_projects",
                    DbType = DbType.String,
					Value = product.CustomersAndProjects
                });
				cmd.Parameters.Add(new MySqlParameter
                {
					ParameterName = "@services",
                    DbType = DbType.String,
					Value = product.Services
                });
				cmd.Parameters.Add(new MySqlParameter
                {
					ParameterName = "@version",
                    DbType = DbType.String,
					Value = product.Version
                });
				cmd.Parameters.Add(new MySqlParameter
                {
					ParameterName = "@submitter",
                    DbType = DbType.String,
					Value = product.Submitter
                });
                cmd.ExecuteNonQuery();
			}
		}

		public async Task<bool> UploadAsync(IFormFile image)
        {
            try
            {
				CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ImagesConnectionString);

                // Create a blob client for interacting with the blob service.
                blobClient = storageAccount.CreateCloudBlobClient();
                blobContainer = blobClient.GetContainerReference(blobContainerName);
                await blobContainer.CreateIfNotExistsAsync();

                // To view the uploaded blob in a browser, you have two options. The first option is to use a Shared Access Signature (SAS) token to delegate  
                // access to the resource. See the documentation links at the top for more information on SAS. The second approach is to set permissions  
                // to allow public access to blobs in this container. Comment the line below to not use this approach and to use SAS. Then you can view the image  
                // using: https://[InsertYourStorageAccountNameHere].blob.core.windows.net/webappstoragedotnet-imagecontainer/FileName 
                await blobContainer.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                CloudBlockBlob cloudBlockBlob = blobContainer.GetBlockBlobReference(image.FileName);
                await cloudBlockBlob.UploadFromStreamAsync(image.OpenReadStream());
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            return true;
        }

        public Uri GetBlobURI(string name) 
		{
			// Retrieve storage account information from connection string
			// How to create a storage connection string - http://msdn.microsoft.com/en-us/library/azure/ee758697.aspx
			//CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ImagesConnectionString);

			// Create a blob client for interacting with the blob service.
			//blobClient = storageAccount.CreateCloudBlobClient();
			//blobContainer = blobClient.GetContainerReference(blobContainerName);
			//await blobContainer.CreateIfNotExistsAsync();

			// To view the uploaded blob in a browser, you have two options. The first option is to use a Shared Access Signature (SAS) token to delegate  
			// access to the resource. See the documentation links at the top for more information on SAS. The second approach is to set permissions  
			// to allow public access to blobs in this container. Comment the line below to not use this approach and to use SAS. Then you can view the image  
			// using: https://[InsertYourStorageAccountNameHere].blob.core.windows.net/webappstoragedotnet-imagecontainer/FileName 
			//         await blobContainer.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

			//var blob = blobContainer.GetBlobReference(name);
			//return blob.Uri;
			return new Uri(bloburl + name);
		}
        
		public async Task<List<SalesDocument>> GetFiles(string key) {
			List<SalesDocument> cloudFiles = new List<SalesDocument>();

			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ImagesConnectionString);
			CloudFileClient cloudFileClient = storageAccount.CreateCloudFileClient();
			CloudFileShare cloudFileShare = cloudFileClient.GetShareReference("salesassistantfiles");
			await cloudFileShare.CreateIfNotExistsAsync();
			CloudFileDirectory rootDirectory = cloudFileShare.GetRootDirectoryReference();

			CloudFileDirectory fileDirectory = rootDirectory.GetDirectoryReference(key);
			await fileDirectory.CreateIfNotExistsAsync();

			List<IListFileItem> listedFileItems = new List<IListFileItem>();

			FileContinuationToken token = null;
			do
			{
				FileResultSegment resultSegment = await fileDirectory.ListFilesAndDirectoriesSegmentedAsync(token);
				token = resultSegment.ContinuationToken;

				foreach (IListFileItem listResultItem in resultSegment.Results)
                {
                     listedFileItems.Add(listResultItem);
                }

			}
			while (token != null);

			listedFileItems.ForEach((IListFileItem obj) =>
			{
				CloudFile file = (CloudFile) obj;
				SalesDocument salesDocument = new SalesDocument();
				salesDocument.cloudFile = file;
				salesDocument.name = Path.GetFileNameWithoutExtension(file.Name);
				salesDocument.downloadLink = GetDownloadLink(file);
				cloudFiles.Add(salesDocument);
			});
   
            return cloudFiles;
		}

		public async Task<List<SalesDocument>> GetProductFiles(string key, string category)
        {
            List<SalesDocument> cloudFiles = new List<SalesDocument>();

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ImagesConnectionString);
            CloudFileClient cloudFileClient = storageAccount.CreateCloudFileClient();
            CloudFileShare cloudFileShare = cloudFileClient.GetShareReference("salesassistantfiles");
            await cloudFileShare.CreateIfNotExistsAsync();
            CloudFileDirectory rootDirectory = cloudFileShare.GetRootDirectoryReference();

			CloudFileDirectory productsDirectory = rootDirectory.GetDirectoryReference("Products");
			await productsDirectory.CreateIfNotExistsAsync();

			CloudFileDirectory fileDirectory = productsDirectory.GetDirectoryReference(key);
            await fileDirectory.CreateIfNotExistsAsync();
            
			CloudFileDirectory categoryDirectory = fileDirectory.GetDirectoryReference(category);
			await categoryDirectory.CreateIfNotExistsAsync();

            List<IListFileItem> listedFileItems = new List<IListFileItem>();

            FileContinuationToken token = null;
            do
            {
				FileResultSegment resultSegment = await categoryDirectory.ListFilesAndDirectoriesSegmentedAsync(token);
                token = resultSegment.ContinuationToken;

                foreach (IListFileItem listResultItem in resultSegment.Results)
                {
                    listedFileItems.Add(listResultItem);
                }

            }
            while (token != null);

            listedFileItems.ForEach((IListFileItem obj) =>
            {
                CloudFile file = (CloudFile)obj;
                SalesDocument salesDocument = new SalesDocument();
                salesDocument.cloudFile = file;
                salesDocument.name = Path.GetFileNameWithoutExtension(file.Name);
                salesDocument.downloadLink = GetDownloadLink(file);
                cloudFiles.Add(salesDocument);
            });

            return cloudFiles;
        }

		public string GetDownloadLink(CloudFile file)
        {
			SharedAccessFilePolicy policy = new SharedAccessFilePolicy()
            {
                Permissions = SharedAccessFilePermissions.Read,
                SharedAccessExpiryTime = DateTime.UtcNow.AddHours(12),
            };
			SharedAccessFileHeaders headers = new SharedAccessFileHeaders()
            {
				ContentDisposition = string.Format("attachment;filename=\"{0}\"", file.Name),
            };

			var sasToken = file.GetSharedAccessSignature(policy, headers);
			return file.Uri.AbsoluteUri + sasToken;
        }

		public async Task<bool> UploadDocumentAsync(IFormFile document, string key)
        {
            try
            {
				CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ImagesConnectionString);

				CloudFileClient cloudFileClient = storageAccount.CreateCloudFileClient();
                CloudFileShare cloudFileShare = cloudFileClient.GetShareReference("salesassistantfiles");
                await cloudFileShare.CreateIfNotExistsAsync();
                CloudFileDirectory rootDirectory = cloudFileShare.GetRootDirectoryReference();

                CloudFileDirectory fileDirectory = rootDirectory.GetDirectoryReference(key);
                await fileDirectory.CreateIfNotExistsAsync();

				var tempPath = Path.GetTempFileName();
				using (var stream = new FileStream(tempPath, FileMode.Create))
				{
					await document.CopyToAsync(stream);
				}

				using (var stream = new FileStream(tempPath, FileMode.Open)) 
				{
					CloudFile newCloudFile = fileDirectory.GetFileReference(document.FileName);
					await newCloudFile.UploadFromStreamAsync(stream);
				}
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            return true;
        }

		public async Task<bool> UploadProductDocumentAsync(IFormFile document, string key, string category)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ImagesConnectionString);

                CloudFileClient cloudFileClient = storageAccount.CreateCloudFileClient();
                CloudFileShare cloudFileShare = cloudFileClient.GetShareReference("salesassistantfiles");
                await cloudFileShare.CreateIfNotExistsAsync();
				CloudFileDirectory rootDirectory = cloudFileShare.GetRootDirectoryReference();

                CloudFileDirectory productsDirectory = rootDirectory.GetDirectoryReference("Products");
                await productsDirectory.CreateIfNotExistsAsync();

                CloudFileDirectory fileDirectory = productsDirectory.GetDirectoryReference(key);
                await fileDirectory.CreateIfNotExistsAsync();

				CloudFileDirectory categoryDirectory = fileDirectory.GetDirectoryReference(category);
				await categoryDirectory.CreateIfNotExistsAsync();

                var tempPath = Path.GetTempFileName();
                using (var stream = new FileStream(tempPath, FileMode.Create))
                {
                    await document.CopyToAsync(stream);
                }

                using (var stream = new FileStream(tempPath, FileMode.Open))
                {
					CloudFile newCloudFile = categoryDirectory.GetFileReference(document.FileName);
                    await newCloudFile.UploadFromStreamAsync(stream);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return true;
        }

		public async void SignIn(HttpContext httpContext, User user, bool isPersistent = false)
        {
            using (var con = new MySqlConnection(ConnectionString))
            {

                User dbUser = null;
                using (MySqlConnection connection = GetConnection())
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand("SELECT * FROM salesassistantdb.users WHERE salesassistantdb.users.email = \"" + user.Email
                            + "\" AND salesassistantdb.users.password = \"" + user.Password + "\"", connection);
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user = new User()
                            {
                                Id = reader["id"] is DBNull ? 0 : reader.GetInt32("id"),
                                Email = reader["email"] is DBNull ? null : reader.GetString("email"),
                                Access = reader["access"] is DBNull ? false : reader.GetBoolean("access")
                            };
                            dbUser = user;
                        }
                    }

                }

                ClaimsIdentity identity = new ClaimsIdentity(this.GetUserClaims(dbUser), CookieAuthenticationDefaults.AuthenticationScheme);
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
            }
        }

        public async void SignOut(HttpContext httpContext)
        {
            await httpContext.SignOutAsync();
        }

        private IEnumerable<Claim> GetUserClaims(User user)
        {
            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.Email, user.Email));
            claims.AddRange(this.GetUserRoleClaims(user));
            return claims;
        }

        private IEnumerable<Claim> GetUserRoleClaims(User user)
        {
            List<Claim> claims = new List<Claim>();
            
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Email));
			if (user.Access == true) {
				claims.Add(new Claim(ClaimTypes.Role, "Admin"));
			}
            return claims;
        }
	}
}
