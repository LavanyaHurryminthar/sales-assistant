﻿using System;
using System.Collections.Generic;

namespace salesassistant.Model
{
    public class CoE
    {
		public long Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string ExcoName { get; set; }
		public List<Product> productList { get; set; }
		public Uri ImageUri { get; set; }

		public List<string> DescriptionParts { get; set; }
    }
}
