﻿using System;
using System.Collections.Generic;

namespace salesassistant.Model
{
    public class ExCo
    {
		public long Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
        public List<CoE> coeList { get; set; }
		public List<string> NameParts { get; set; }
		public Uri ImageUri { get; set; }

    }
}
