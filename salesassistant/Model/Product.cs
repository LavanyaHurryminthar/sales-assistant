﻿using System;
using System.Collections.Generic;

namespace salesassistant.Model
{
    public class Product
    {
		public long Id { get; set; }
		public string ExcoName { get; set; }
		public string COEName { get; set; }
		public string ServiceName { get; set; }
		public string OwnerOneName { get; set; }
		public string OwnerOneEmail { get; set; }
		public string OwnerOneNumber { get; set; }
		public string OwnerTwoName { get; set; }
		public string OwnerTwoEmail { get; set; }
		public string OwnerTwoNumber { get; set; }
		public List<string> indexes { get; set; }

		public string ElevatorPitch { get; set; }
		public string ServiceOfferingDescription{ get; set; }
		public string ProblemSolved { get; set; }
		public string ValueProposition { get; set; }
		public string CustomersAndProjects { get; set; }
		public string Services { get; set; }

		public int Version { get; set; }
		public string Submitter { get; set; }
		public Uri ImageUri { get; set; }
    }
}
