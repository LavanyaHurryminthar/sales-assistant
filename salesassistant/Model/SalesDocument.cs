﻿using System;
using Microsoft.WindowsAzure.Storage.File;

namespace salesassistant.Model
{
    public class SalesDocument
    {
		public CloudFile cloudFile { get; set; }
		public string name { get; set; }
		public string type { get; set; }
		public string downloadLink { get; set; }
    }
}
