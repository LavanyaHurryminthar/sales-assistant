﻿using System;
using Microsoft.AspNetCore.Identity;

namespace salesassistant.Model
{

	public class User : IdentityUser
    {
		public new long Id { get; set; }
		override public string Email { get; set; }
		public string Password { get; set; }
		public bool Access { get; set; }
    }
}
