﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using salesassistant.Database;
using salesassistant.Model;

namespace salesassistant.Pages
{
	[Authorize(Roles = "Admin")]
    public class AddEntryModel : PageModel
    {
		[BindProperty]
        public IFormFile FileUpload { get; set; }

		public List<ExCo> ExCos { get; set; }

		[BindProperty]
		public Product Product { get; set; }

		[BindProperty]
        public Uri ImageUri { get; private set; }

		public List<string> ExcoOptions { get; set; }

		public string ServiceName { get; set; }

        public void OnGet(string key = "", string service_name = "")
        {
			if (!"".Equals(service_name))
			{
				ServiceName = service_name;
			}
			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
			if (!"".Equals(key))
			{
				ImageUri = context.GetBlobURI(key);
			}
			ExcoOptions = new List<string>();
			ExCos = context.GetExCos();
			ExCos.ForEach((ExCo obj) =>
			{
				ExcoOptions.Add(obj.Name);
			});
        }

		public async Task<IActionResult> OnPostAsync()
        {
            System.Diagnostics.Debug.WriteLine("adding new record");
			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
            if (!ModelState.IsValid)
            {
                return Page();
            }

			//context.AddMessage(Message.MessageText, Message.Name, DateTime.Now.ToShortDateString());
			if (ImageUri != null) 
			{
				Product.ImageUri = ImageUri;
			}
			context.AddProduct(Product);
			return RedirectToPage("/AddEntry", new { service_name = Product.ServiceName });
        }

		public async Task<IActionResult> OnPostUploadImageAsync()
        {
            System.Diagnostics.Debug.WriteLine("Uploading image : " + FileUpload.FileName);
			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
            await context.UploadAsync(FileUpload);
			return RedirectToPage("/AddEntry", new { key = FileUpload.FileName });
        }
    }
}
