﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using salesassistant.Database;
using salesassistant.Model;

namespace salesassistant.Pages
{
    public class COEModel : PageModel
    {
		public static string Key { get; set; }
		public CoE coe { get; set; }
		public List<SalesDocument> salesDocuments { get; set; }

		[BindProperty]
        public IFormFile FileUpload { get; set; }

        public void OnGet(string key)
        {
			Key = key;

			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
			coe = context.GetCoE(key);
			coe.productList = context.GetProducts(coe.Name);

			salesDocuments = context.GetFiles(key).Result;
        }

		public async Task<IActionResult> OnPostUploadDocumentAsync()
        {
            SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
            await context.UploadDocumentAsync(FileUpload, Key);
            return RedirectToPage("/COE", new { key = Key });
        }
    }
}
