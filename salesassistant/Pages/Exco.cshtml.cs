﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.WindowsAzure.Storage.File;
using salesassistant.Database;
using salesassistant.Model;

namespace salesassistant.Pages
{
    public class ExcoModel : PageModel
    {

		public static string Key { get; set; }

		public ExCo Exco { get; set; }

		bool isAuthenticated { get; }

		public List<SalesDocument> salesDocuments { get; set; }

		[BindProperty]
        public IFormFile FileUpload { get; set; }

        [Authorize]
        public void OnGet(string key)
        {
			Key = key;

			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
			Exco = context.GetExCo(key);
			Exco.coeList = context.GetCoEs(Exco.Name);
            
			salesDocuments = context.GetFiles(key).Result;
			// Getting User Identity: Authentication etc...
			// User.Identity.Name;
		}

		public async Task<IActionResult> OnPostUploadDocumentAsync()
        {
			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
			await context.UploadDocumentAsync(FileUpload, Key);
			return RedirectToPage("/Exco", new { key = Key });
        }
    }
}
