﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using salesassistant.Database;
using salesassistant.Model;

namespace salesassistant.Pages
{
	
    public class IndexModel : PageModel
    {
		public String Name { get; set; }
        public String Description { get; set; }
        public List<ExCo> excoList { get; set; }

        public void OnGet()
        {
            SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
            excoList = context.GetExCos();
        }


    }
}
