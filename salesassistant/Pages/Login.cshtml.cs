﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using salesassistant.Database;
using salesassistant.Model;

namespace salesassistant.Pages
{
    public class LoginModel : PageModel
    {
		//public LoginModel(UserManager userManager)
		public LoginModel() {
			//this._userManager = userManager;
		}

		//UserManager _userManager;


		[BindProperty]
		public string Username { get; set; }

		[BindProperty]
		public string Password { get; set; }

		[AllowAnonymous]
		public void OnGet()
        {
        }

		public IActionResult OnPostUserLogin()
        {
            SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
		    //User user = context.GetUserPassword(Username, Password);

			//if (user != null) {
			context.SignIn(HttpContext, new User
			{
				Email = Username,
				Password = Password
			});
			return RedirectToPage("/index");
			//} else {
			//	return RedirectToPage("/login", new { login = "failed"});
			//}
        }
    }
}
