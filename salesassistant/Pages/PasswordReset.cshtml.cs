﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using salesassistant.Database;
using salesassistant.Model;

namespace salesassistant.Pages
{
    public class PasswordResetModel : PageModel
    {
		public static string UserEmail { get; set; }
		[BindProperty]
		public string NewPassword { get; set; }

		public void OnGet(string useremail)
        {
		    UserEmail = useremail;
        }
        
		public IActionResult OnPostResetPassword()
        {
            SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
            var Users = context.GetUsers();
			User toggledUser = Users.Find(user => user.Email.Equals(UserEmail));
			toggledUser.Password = NewPassword;
			context.UpdateUserPassword(toggledUser);
            return RedirectToPage("/Users");
        }
    }
}
