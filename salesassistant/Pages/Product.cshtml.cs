﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using salesassistant.Database;
using salesassistant.Model;

namespace salesassistant.Pages
{
	public class ProductModel : PageModel
	{
        
		public static string Key { get;set; }
		public Product product { get; set; }
		public List<SalesDocument> presentationDocuments { get; set; }
		public List<SalesDocument> questionnaireDocuments { get; set; }
		public List<SalesDocument> pricingDocuments { get; set; }
		public List<SalesDocument> brochureDocuments { get; set; }


		[BindProperty]
        public IFormFile FileUpload { get; set; }

		[BindProperty]
		public string FileCategory { get; set; }

        public void OnGet(string key)
        {
			Key = key;
			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
			product = context.GetProduct(key);

			presentationDocuments = context.GetProductFiles(key, "Presentations").Result;
			questionnaireDocuments = context.GetProductFiles(key, "Questionnaire").Result;
			pricingDocuments = context.GetProductFiles(key, "Pricing").Result;
			brochureDocuments = context.GetProductFiles(key, "Brochure").Result;
        }

		public async Task<IActionResult> OnPostUploadDocumentAsync()
        {
			if (FileUpload == null || FileCategory == null) {
				return RedirectToPage("/Product", new { key = Key });
			}
            SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
			await context.UploadProductDocumentAsync(FileUpload, Key, FileCategory);
            return RedirectToPage("/Product", new { key = Key });
        }
    }
}
