﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using salesassistant.Database;
using salesassistant.Model;

namespace salesassistant.Pages
{
    public class TablesModel : PageModel
    {
        
		public string currentExco { get; set; }
		public List<CoE> coes = new List<CoE>();
		public List<ExCo> excos = new List<ExCo>();
		public List<Product> products = new List<Product>();

		public string GetActiveTab(string tabName)
		{
			if (tabName.Equals(currentExco)) return "active";
			return "";
		}

        public void OnGet(string exco = "Chief Digital Officer")
        {
			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
			excos = context.GetExCos();
			currentExco = exco;

			coes = context.GetCoEs(currentExco);
			coes.OrderBy(coe => coe.Name);
			    
			products = context.GetProductsInExco(currentExco);
        }

        public List<Product> GetProductsForCoE(string COEName)
		{
			return products.FindAll(product => product.COEName.ToLower().Equals(COEName.ToLower()));
		}
    }
}
