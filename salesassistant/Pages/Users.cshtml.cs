﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using salesassistant.Database;
using salesassistant.Model;

namespace salesassistant.Pages
{
	[Authorize(Roles = "Admin")]
    public class UsersModel : PageModel
    {
		public List<User> Users { get; set; }
		[BindProperty]
		public string NewUserEmail { get; set; }
		[BindProperty]
        public string NewUserPassword { get; set; }
		[BindProperty]
        public string ChangedPassword { get; set; }
		[BindProperty]
		public Boolean NewUserAccess { get; set; }

        public void OnGet()
        {
			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
			this.Users = context.GetUsers();
        }

		public IActionResult OnPostToggleUser(string email)
		{
			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
			this.Users = context.GetUsers();
			User toggledUser = Users.Find(user => user.Email.Equals(email));
            toggledUser.Access = !toggledUser.Access;
			context.UpdateUsers(this.Users);
			return RedirectToPage("/Users");
		}

		public IActionResult OnPostDeleteUser(string email)
        {
            SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
            this.Users = context.GetUsers();
			User userToDelete = Users.Find(user => user.Email.Equals(email));
            context.DeleteUser(userToDelete);
            return RedirectToPage("/Users");
        }
        
		public IActionResult OnPostResetPassword(string email) {
			return RedirectToPage("/PasswordReset", new { useremail = email});
		}
        
        public IActionResult OnPostSaveNewUser()
		{
			User user = new User()
			{
				Email = NewUserEmail,
				Password = NewUserPassword,
				Access = NewUserAccess
			};
			SalesAssistantContext context = HttpContext.RequestServices.GetService(typeof(SalesAssistantContext)) as SalesAssistantContext;
			context.SaveUser(user);
			return RedirectToPage("/Users");
		}
    }
}
