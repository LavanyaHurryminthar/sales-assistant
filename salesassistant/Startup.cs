using System;
using System.Linq;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using salesassistant.Database;
using salesassistant.Model;

namespace salesassistant
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
			        .AddRazorPagesOptions(options => {
				        options.Conventions.AllowAnonymousToPage("/Login");
						options.Conventions.AllowAnonymousToPage("/Error");
        				options.Conventions.AuthorizePage("/Index");
        				options.Conventions.AuthorizePage("/COE");
        				options.Conventions.AuthorizePage("/Exco");
				        options.Conventions.AuthorizePage("/Product");
        				options.Conventions.AuthorizePage("/Users");
        				options.Conventions.AuthorizePage("/Tables");
        				options.Conventions.AuthorizePage("/Users");
			});

			services.Add(new ServiceDescriptor(typeof(SalesAssistantContext), new SalesAssistantContext(
                Configuration.GetConnectionString("DefaultConnection"),
                Configuration.GetConnectionString("ImagesConnection"))));

			services.AddAuthentication(
                    CookieAuthenticationDefaults.AuthenticationScheme)
			        .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme,
                    options =>
                    {
                        options.LoginPath = "/Login";
                        options.LogoutPath = "/Login";
                    }
            );

			// authentication 
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            });

			//services.AddTransient(
				//m => new UserManager(
          //          Configuration
          //              .GetValue<string>(
						    //Configuration.GetConnectionString("DefaultConnection") //this is a string constant
                //        )
                //    )
                //);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
			app.UseAuthentication();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller}/{action=Index}/{id?}");
			});

        }
    }
}
